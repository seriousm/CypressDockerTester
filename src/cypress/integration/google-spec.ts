describe('google.com test', () => {
  it('has a search box', () => {
    cy.visit('https://www.google.com');

    cy.get('input[name="q"]')
      .type("cypress.io");

    cy.get('input[name="btnK"]')
      .click();

    cy.location()
      .its('href')
      .should('contain', 'q=cypress.io');
  });
});