describe('webapp test', () => {
  it('page content exists', () => {
    cy.visit('/');

    cy.contains('h2', 'Web Portal');

    cy.contains('a', 'Next page')
      .click();
    
    cy.contains('Welcome to the rabbit hole');
  });
});