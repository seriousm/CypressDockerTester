SET LOCAL_NAME=cypress/test

echo "Running tests against %LOCAL_NAME%"
docker run --platform linux -it --rm -v "C:\dev\_playground\CypressTest\src:/test" -w "/test" %LOCAL_NAME%
