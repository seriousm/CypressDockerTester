FROM cypress/base:10

RUN wget https://github.com/jwilder/dockerize/releases/download/v0.6.1/dockerize-linux-amd64-v0.6.1.tar.gz \
    && tar -C /usr/local/bin -xzvf dockerize-linux-amd64-v0.6.1.tar.gz \
    && rm dockerize-linux-amd64-v0.6.1.tar.gz

WORKDIR /cypress

COPY cypress-package.json package.json
COPY cypress-package-lock.json package-lock.json
RUN npm install --no-audit

WORKDIR /cypress/test
VOLUME [ "/cypress/test" ]

RUN ../node_modules/.bin/cypress verify

CMD ["../node_modules/.bin/cypress", "run"]
